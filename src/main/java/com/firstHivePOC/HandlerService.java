package com.firstHivePOC;

import org.ehcache.Cache;

public class HandlerService {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args) throws InterruptedException {
	try {
		MyCacheImplementation cacheImpl = new MyCacheImplementation();
		Cache myCache = createCache(cacheImpl, 10);
		myCache = putCache(myCache, "2", "Firsthive2");
		myCache = putCache(myCache, "1", "Firsthive1");
		myCache = putCache(myCache, "3", "Firsthive3");
		getValue(myCache,"1");
	//	Thread.sleep(2000); // 2 seconds
		getValue(myCache,"2");
//		Thread.sleep(4000); // 4 seconds, 6 seconds lapsed
		getValue(myCache,"2");
	//	Thread.sleep(3000); // 3 seconds, 9 seconds lapsed
		getValue(myCache,"2");
		Cache testCache = createCache(cacheImpl,15);
		testCache = putCache(testCache, "1", "Firsthive_TestCache");
		//getValue(testCache, "2");
	//	Thread.sleep(4000); // 4 seconds, 13 seconds lapsed
		getValue(myCache,"2");
		
		getValue(myCache,"1");
		getValue(myCache,"3");
		getValue(testCache, "1");
		System.out.println(testCache.hashCode());
		System.out.println(myCache.hashCode());
	}
	catch(Exception e){
		e.printStackTrace();
	}
		
	}
	
	public static Cache createCache(MyCacheImplementation cacheImpl, int timelimit) {
		
		Cache myCache = cacheImpl.buildInMemoryCache("TestCache", String.class, String.class, timelimit);
		return myCache;
	}
	
	public static Cache putCache(Cache myCache, String key, String value) {
		
		myCache.put(key, value);
		return myCache;
	}
	

	public static void getValue(Cache myCache, String key) {
		System.out.println(myCache.get(key));
		
	}
	
	
}

