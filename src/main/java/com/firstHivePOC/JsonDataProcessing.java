package com.firstHivePOC;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

public class JsonDataProcessing {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		generateWSAutomationCaption("{\"wstype\":\"assignpoint\",\"prdId\":null,\"amt\":\"0.05\",\"debitfromshopper\":0,\"remark\":\"testArun\",\"limit\":\"1\",\"pointtype\":\"flexible\",\"fieldname\":\"2\",\"datefield\":\"182992\",\"fromdate\":\"2020-10-01\",\"todate\":\"2020-10-27\",\"other\":[{\"id\":\"wscondrule0\",\"fieldId\":\"2\",\"fieldName\":\"Email\",\"fieldType\":\"string\",\"condition\":\"CON\",\"andorcond\":\"AND\",\"value\":\"a\"}]}");
//		generateWSAutomationCaption("{\"wstype\":\"assignpoint\",\"prdId\":null,\"amt\":\"0.05\",\"debitfromshopper\":0,\"remark\":\"testArun\",\"limit\":\"0\",\"pointtype\":\"flexible\",\"fieldname\":\"2\",\"datefield\":\"\",\"fromdate\":\"\",\"todate\":\"\",\"other\":[{\"id\":\"wscondrule0\",\"fieldId\":\"2\",\"fieldName\":\"Email\",\"fieldType\":\"string\",\"condition\":\"CON\",\"andorcond\":\"AND\",\"value\":\"a\"},{\"id\":\"wscondrule1\",\"fieldId\":\"4\",\"fieldName\":\"Mobile\",\"fieldType\":\"string\",\"condition\":\"IN\",\"andorcond\":\"OR\",\"value\":\"7148\"}]}");
		//generateWSAutomationCaption("{\"wstype\":\"assignpoint\",\"prdId\":null,\"amt\":\"0.05\",\"debitfromshopper\":0,\"remark\":\"aruntest2\",\"limit\":\"0\",\"pointtype\":\"fixed\",\"fieldname\":\"2\",\"datefield\":\"\",\"fromdate\":\"\",\"todate\":\"\",\"other\":[{\"id\":\"wscondrule0\",\"fieldId\":\"2\",\"fieldName\":\"Email\",\"fieldType\":\"string\",\"condition\":\"\",\"andorcond\":\"AND\",\"value\":\"\"}]}");
		generateWSAutomationCaption("{\"wstype\":\"assignpoint\",\"prdId\":null,\"amt\":\"20\",\"debitfromshopper\":0,\"remark\":\"\",\"limit\":\"0\",\"pointtype\":\"flexible\",\"fieldname\":\"19\",\"datefield\":\"\",\"fromdate\":\"\",\"todate\":\"\",\"other\":[{\"id\":\"wscondrule0\",\"fieldId\":\"2\",\"fieldName\":\"Email\",\"fieldType\":\"string\",\"condition\":\"IN\",\"andorcond\":null,\"value\":\"D\"}]}");
	//generateWSAutomationCaption("{\"wstype\":\"assignpoint\",\"remark\":\"testArun4\",\"limit\":\"\",\"pointtype\":\"hybrid\",\"grouparray\":[{\"hybridgroupcond\":\"\",\"pointtypeH\":\"hybridflexible\",\"fieldCalculate\":\"Maximum\",\"fieldCalculateVal\":\"188\",\"datefield\":\"182023\",\"fromDate\":\"2020-10-05\",\"toDate\":\"2020-10-13\",\"amt\":\"0.7\",\"other\":[{\"id\":0,\"fieldId\":\"2\",\"fieldName\":\"Email\",\"fieldType\":\"string\",\"condition\":\"CON\",\"andorcond\":null,\"value\":\"k\"}]},{\"hybridgroupcond\":\"subtract\",\"pointtypeH\":\"hybridflexible\",\"fieldCalculate\":\"tCount\",\"fieldCalculateVal\":\"4\",\"datefield\":\"182773\",\"fromDate\":\"2020-10-01\",\"toDate\":\"2020-10-27\",\"amt\":\"0.9\",\"other\":[{\"id\":0,\"fieldId\":\"19\",\"fieldName\":\"First Name\",\"fieldType\":\"string\",\"condition\":\"IS\",\"andorcond\":\"AND\",\"value\":\"hfh\"}]},{\"hybridgroupcond\":\"add\",\"pointtypeH\":\"hybridfixed\",\"fieldCalculate\":\"\",\"fieldCalculateVal\":\"\",\"datefield\":\"\",\"fromDate\":\"\",\"toDate\":\"\",\"amt\":\"1\",\"other\":[]}]}");
		generateWSAutomationCaption("{\"wstype\":\"assignpoint\",\"prdId\":null,\"amt\":\"4\",\"debitfromshopper\":0,\"remark\":\"\",\"limit\":\"0\",\"pointtype\":\"flexible\",\"fieldname\":\"2450\",\"datefield\":\"2672\",\"fromdate\":\"2020-10-01\",\"todate\":\"2020-12-31\",\"other\":[{\"id\":\"wscondrule0\",\"fieldId\":\"2451\",\"fieldName\":\"Transaction Product Name\",\"fieldType\":\"string\",\"condition\":\"CON\",\"andorcond\":\"AND\",\"value\":\"A-KWIK\"}]}");
	}

	public static String generateWSAutomationCaption(String jsonData) {
		
		Map<String, String> condArr = new HashMap<String, String>();
		condArr.put("IS", "Is");
		condArr.put("IN", "Is Not");
		condArr.put("CON", "Contains");
		condArr.put("DNC", "Does Not Contains");
		condArr.put("GT", "Greater Than ");
		condArr.put("GTE", "Greater Than or Equals To ");
		condArr.put("LT", "Less Than ");
		condArr.put("LTE", "Less Than or Equals To ");
		condArr.put("IOF", "Is One Of ");
		condArr.put("tCount", "Total Count of");
		condArr.put("dCount", "Unique Count of");
		condArr.put("T-D", "Is Days After Today ");
		condArr.put("T+D", "Is Days Before Today ");
		condArr.put("CD-D", "Excluding Days From Today ");
		condArr.put("CD+D", "Including Days After Today ");
		condArr.put("Maximum","Maximum of");
		condArr.put("Minimum", "Minimum Of");
		condArr.put("Average", "Average Of");
		condArr.put("Cont", "Constant");
		condArr.put("Sumof", "Sum Of");
		JSONObject wsData = null;
		String captionCondition ="";
		String captionConditionH ="<br>";
		String finalData= " <br>Based on below condition<br> ";
		String type ="";
		int count =0;
		System.out.println("Inside generateWSAutomationCaption");
		try {
			wsData = new JSONObject(jsonData);
			type=wsData.getString("pointtype");
			
			if(type.equalsIgnoreCase("flexible")) {
				String joinCondition = "";
				//System.out.println(wsData);
				String fromDate = wsData.getString("fromdate");
				String toDate = wsData.getString("todate"); 
				String dataField = wsData.getString("fieldname");
				String amt = wsData.getString("amt");
				System.out.println(wsData.get("other")); 
				
				if(wsData.has("other")) {
					JSONArray conditionData = wsData.getJSONArray("other");
				
				for(int i=0;i<conditionData.length();i++) {
					System.out.println(conditionData.get(i));
					JSONObject conditionArr = conditionData.getJSONObject(i);
					joinCondition = i>0? "<br>"+conditionArr.get("andorcond").toString()+"<br> " : "";
						if(conditionArr.get("value").toString().length()>0) {
							captionCondition+=joinCondition;
							captionCondition += conditionArr.get("fieldName").toString() + " "+ condArr.get(conditionArr.get("condition")) +" "+ conditionArr.get("value"); 			
						}
				}
				}
				finalData=  "<br> Assign "+amt+" worldswipe points Per Unit on value "+dataField+finalData+ "( "+ captionCondition +" <br>From Date: "+fromDate+" To Date: "+toDate+")";
				System.out.println(finalData);
			}
			else if(type.equalsIgnoreCase("hybrid")) {
			
				JSONArray hybridData = wsData.getJSONArray("grouparray");
				String hybridGroupCond ="";
				String hybridCaptionCondition ="";
				for(int j=0;j<hybridData.length();j++) {
					count++;
					//System.out.println(">>>Hybrid Data"+hybridData.get(j));
					JSONObject hybridWsData = hybridData.getJSONObject(j);
					String typeH = hybridWsData.getString("pointtypeH");
					hybridGroupCond = hybridWsData.getString("hybridgroupcond").length()>0? hybridWsData.getString("hybridgroupcond") +"<br>": "";
					if(typeH.equalsIgnoreCase("hybridflexible")) {
						captionConditionH="";
						String joinCondition = "";	
						//System.out.println(wsData.get("other"));
					//	System.out.println(hybridWsData);
						String fromDate = hybridWsData.getString("fromDate");
						String toDate = hybridWsData.getString("toDate");
						String amt = hybridWsData.getString("amt");
						String dataField = hybridWsData.getString("fieldCalculateVal");
						String dataCalc = hybridWsData.getString("fieldCalculate");
						JSONArray conditionData = hybridWsData.getJSONArray("other");
						for(int i=0;i<conditionData.length();i++) {
						//	System.out.println(conditionData.get(i));
							JSONObject conditionArr = conditionData.getJSONObject(i);
							
								if(conditionArr.get("value").toString().length()>0) {
									if(i>0)captionConditionH +=joinCondition;
									captionConditionH += conditionArr.get("fieldName").toString() + " "+ condArr.get(conditionArr.get("condition")) +" "+ conditionArr.get("value"); 
								}
								joinCondition = "<br>"+conditionArr.get("andorcond").toString()+"<br>";
						}
						hybridCaptionCondition+= hybridGroupCond.toUpperCase()+"<br> ( Assign "+ amt+" wroldswipe points Per Unit on "+condArr.get(dataCalc)+" "+dataField+"<br> Based on Below Conditions<br> "+captionConditionH+" <br> From Date:"+fromDate+" To Date:"+toDate+"  )<br> <br>";
						System.out.println(hybridCaptionCondition);
					}
					
					if(typeH.equalsIgnoreCase("hybridfixed")) {
						
						hybridCaptionCondition += hybridGroupCond.toUpperCase()+"( Assign "+hybridWsData.get("amt")+" worldswipe points -Fixed ) <br><br>";
					}
				}	
				 finalData= count>1 ?  hybridCaptionCondition.substring(0, hybridCaptionCondition.length()-9) :  hybridCaptionCondition.substring(0, hybridCaptionCondition.length()-4);
				
			System.out.println(finalData);
			}
			else {
				System.out.println("Type is fixed");
				finalData="";
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
		
	return finalData;
	}
	
}
