package com.firstHivePOC;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.json.JSONArray;
import org.json.JSONObject;

import net.sf.uadetector.OperatingSystem;
import net.sf.uadetector.UserAgentStringParser;
import net.sf.uadetector.service.UADetectorServiceFactory;

public class JsonParserService {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		// System.out.println(parseString(" "));

		// UseAgentParser("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36
		// (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36");
		// kafkaProducer1();
		// getVisitorData();
		// splitEmail("arun,,");
		// sendReq();
		//jsonDataSla();
		//seting();
		//addMins();
//		 try {
//			sendSMS();
//		} catch (ClientProtocolException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		finalAmount(72);
	}

	public static boolean parseString(String input) {

//		JSONParser parser = new JSONParser();
//		if(!input.trim().equals("")) {
//			
//		
//		try {
//			System.out.println(parser.parse(input).toString());
//			
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//	//		e.printStackTrace();
//			System.out.println("Not a JSON String");
//			return false;
//		}
//	}

//		System.out.println(System.currentTimeMillis());
//		
//		 DateFormat df = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
//	       Date dateobj = new Date();
//	       String currDate = df.format(dateobj).toString().replace(" ", "_").replace(":", "_").replace("-", "");
//	       System.out.println(currDate);
//	       System.out.println(System.currentTimeMillis());
		String smsContent = "{{EMAIL}}test content {{FIRSTNAME}}";

		Pattern patt = Pattern.compile("\\{\\{(.*?)\\}\\}");
		Matcher m = patt.matcher(smsContent);
		while (m.find()) {
			System.out.println(m.group(1));
		}

		return true;
	}

	public static void UseAgentParser(String userAgentData) {
		UserAgentStringParser uaParser = UADetectorServiceFactory.getOnlineUpdatingParser();
		System.out.println("Parser Version:" + uaParser.getDataVersion());

		System.out.println("OS:::" + uaParser.parse(userAgentData).getOperatingSystem());
		OperatingSystem os = uaParser.parse(userAgentData).getOperatingSystem();
		System.out.println(os.getName());
		System.out.println(os.getVersionNumber().toVersionString());
		System.out.println("Name:::" + uaParser.parse(userAgentData).getName());
		System.out.println("Family:::" + uaParser.parse(userAgentData).getFamily());
		System.out.println(uaParser.parse(userAgentData).getVersionNumber().toVersionString());
	}

	public static void jsonMessage() {
		HashMap<String, String> data = new HashMap<String, String>();
		HashMap<String, String> data1 = new HashMap<String, String>();
		List<HashMap<String, String>> dataList = new ArrayList<HashMap<String, String>>();
		data.put("priority", "1");
		data.put("campaigns", "123456,1315256,15478");
		data1.put("priority", "2");
		data1.put("campaigns", "12456,13256,1547358");
		dataList.add(data);
		dataList.add(data1);
		JSONObject jsonData = new JSONObject();
		jsonData.put("data", dataList);
		System.out.println(jsonData);

	}

	public static void kafkaProducer1() throws InterruptedException, ExecutionException {
		System.out.println("Hello");
		String bsServer = "localhost:9092";
		Properties properties = new Properties();
		properties.put("bootstrap.servers", bsServer);
		properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
//		properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9094");
//		properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
//		properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		Producer<String, String> first_producer = new KafkaProducer<String, String>(properties);
		ProducerRecord<String, String> record = new ProducerRecord<String, String>("test_topic", "Hey Arun");
		System.out.println(first_producer.send(record).get());
		// first_producer.flush();
		first_producer.close();
		System.out.println("end");
	}

	public static void getVisitorData() {
		System.out.println("Inside parseUserJsonData");

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDateTime now = LocalDateTime.now();
		// System.out.println(dtf.format(now));
		String currDate = dtf.format(now).toString() + " 00:00:00";
		System.out.println(currDate);
		try {
			RequestConfig requestConfig = RequestConfig.custom()
                    .setCookieSpec(CookieSpecs.STANDARD)
                    .build();
			CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig).build();
			HttpGet httpGet = new HttpGet("http://firsthive.com/engage/piwik/?module=API&method=Live.getVisitorProfile&idSite=165&period=day&date=today&format=json&token_auth=2662ce86a8b564a5c2051b249f39612d&visitorId=1EB51BCE1464A29F");
			CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					httpResponse.getEntity().getContent()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			while ((inputLine = reader.readLine()) != null) {
				response.append(inputLine);
			}
			reader.close();
			httpClient.close();
			writeToFile(response.toString(), "C:\\Users\\fh148\\Documents\\firsthiveIMP\\user_data_6.txt");
			parseData(response.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void writeToFile(String message, String filepath) {

		Path filename = Paths.get(filepath);
		try {
			Files.write(filename, message.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void parseData(String message) {

		JSONObject jsonData = new JSONObject(message);
		JSONObject customDimensions = (JSONObject) jsonData.get("customDimensions");
		JSONArray jArr = customDimensions.getJSONArray("visit");
		for (int i = 0; i < jArr.length(); i++) {
			JSONObject data = jArr.getJSONObject(i);
			// System.out.println(data);
			if (data.get("name").toString().equalsIgnoreCase("Attribute1")) {
				JSONArray emailData = data.getJSONArray("values");
				JSONObject countData = emailData.getJSONObject(0);
				System.out.println(countData);
				System.out.println(countData.get("count"));
			}

		}

	}

	public static void splitEmail(String email) {

		String[] emailIds = email.split(",");
		System.out.println(emailIds.length);

	}

	public static void sendReq() {

//		HttpClient httpClient = new DefaultHttpClient();
//		try {
//			HttpPost request = new HttpPost("https://franklinindia-uat.aiavaamo.com/campaigns");
//			StringEntity params = new StringEntity(
//					"{\r\n  \"conversation\": \"1453678356864\",\r\n  \"idvisitor\": \"34658346975\",\r\n  \"messageId\": \"23568137692847027208\",\r\n  \"campaigns\": [\r\n    {\r\n      \"id\": \"10000112\",\r\n      \"priority\": 1,\r\n      \"cards\": [\r\n        {\r\n          \"title\": \"Here's an opportunity to invest in US companies!\",\r\n          \"description\": \"Franklin U.S. Opportunities Fund invests in leading growth companies across industries and market-capitalizations. Investors with long term horizon seeking geographical diversification to USA may consider this fund.\",\r\n          \"buttons\": {\r\n            \"type\": \"post_message\",\r\n            \"title\": \"Invest Now\",\r\n            \"value\": \"Transact Now\"\r\n          }\r\n        }\r\n      ]\r\n    }\r\n  ]\r\n}");
//			request.addHeader("content-type", "application/json");
//			request.addHeader("Accept", "application/json");
//			request.addHeader("Auth", "5e0b7c5b-e1ca-4a0e-8662-fd6d6cfdc2ca");
//			request.setEntity(params);
//			HttpResponse response = httpClient.execute(request);
//			String statusCode = response.getStatusLine().toString().split(" ")[1];
//		} catch (Exception ex) {
//			// handle exception here
//		} finally {
//			httpClient.getConnectionManager().shutdown();
//		}

	}

	public static void jsonDataSla() {
		JSONObject jsonData = new JSONObject(
				"{\"campaigns\":[{\"cards\":[{\"buttons\":{\"type\":\"web_page\",\"title\":\"Click Here\",\"value\":\"http://u.fhve.co/5gqmpqF\"},\"description\":\"When it comes to a good investment strategy, diversification is key. How about diversifying your portfolio with Franklin Templetons FRANKLIN INDIA EQUITY FUND \",\"title\":\"FRANKLIN INDIA EQUITY FUND\"}],\"id\":\"4351\",\"priority\":\"1\"}],\"messageId\":\"ea7dcc95-18dc-4952-9c25-bfdfdb57ef3a\",\"idvisitor\":\"e3ef56fffb370205\",\"conversation\":\"8070b269f5c475d11d56c65d52515959\"}");
		String data = addSlashesInJSON(jsonData.toString());
		System.out.println(data);
		JSONObject jObj = new JSONObject(data);
		System.out.println(jObj.get("campaigns"));
	}

	public static String addSlashesInJSON(String s) {
//        s = s.replaceAll("\\\\", "\\\\\\\\");
//        s = s.replaceAll("\\n", "\\\\n");
//        s = s.replaceAll("\\r", "\\\\r");
//        s = s.replaceAll("\\00", "\\\\0");
//        s = s.replaceAll("\"", "\\\\\"");
		s = s.replaceAll("'", "\\\\'");

		return s;
	}

	public static void seting() {
		HashSet<Integer> arrset1 = new HashSet<Integer>();
		HashSet<Integer> arrset2 = new HashSet<Integer>();

		// Populating arrset1
		arrset1.add(1);
		arrset1.add(2);
		arrset1.add(3);
		arrset1.add(4);
		arrset1.add(5);
		arrset2.add(1);
		arrset2.add(2);
		arrset2.add(3);
		arrset2.add(4);
		arrset2.add(5);

		HashSet<Integer> arrset3 = new HashSet<Integer>();
		
// Populating arrset1 
//		arrset3.add(1);
//		arrset3.add(2);
//		arrset3.add(3);
//		arrset3.add(6);
//		arrset3.add(7);

		
		arrset1.retainAll(arrset3);
		System.out.println(arrset1);
		
		arrset3.removeAll(arrset1);
		System.out.println(arrset3);
		
		arrset1.addAll(arrset3);
		System.out.println(arrset1);
		
		arrset2.removeAll(arrset1);
		System.out.println(arrset2);
		
		arrset1.clear();
		System.out.println(arrset1);
	
	}
	
	public static void addMins() {
		
//		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		Date d1 = new Date();
//		Calendar cal = Calendar.getInstance();
//		cal.setTime(d1);
//		cal.add(Calendar.MINUTE, 10);
//		String newTime = df.format(cal.getTime());
//		System.out.println(newTime);
		String[] data = new String("5JQHyUSHS0GCrWJNioo5hg.filter1186p1iad2-16614-5FC2DD46-5.0").split(".filter",2);
		
		System.out.println(data[0]);
	}
	
	
	public static void postMessageToLocal() {
		HttpClient client = new HttpClient();
		PostMethod method = new PostMethod("http://localhost:8080/FirstHive_Engage/chatBot/welcomeBotUser");
		//DefaultHttpClient httpClient = new DefaultHttpClient();
		try {
		  //  HttpPost request = new HttpPost(url);
			JSONObject jsonData = new JSONObject();
		    //StringEntity params =new StringEntity(jsonData.toString());
		    Header header = new Header();
		    header.setName("content-type");
		    header.setValue("application/json");
		    method.addRequestHeader(header);
		    header.setName("Auth");
		    header.setValue("Hello");
		    method.addRequestHeader(header);
		    
//		    request.addHeader("content-type", "application/json");
//		    request.addHeader("Accept","application/json");
//		    request.addHeader("Auth", auth);
		    client.executeMethod(method);
		    System.out.println(method.getResponseBodyAsString());
		    
		   // request.setEntity(params);
		    //HttpResponse response = httpClient.execute(request);
		   // System.out.println(response);
		    // handle response here...
		   // respStatusCode = response.getStatusLine().getStatusCode();
		}catch (Exception ex) {
		    // handle exception here
		} finally {
		 //   httpClient.getConnectionManager().shutdown();
		}

	}
	
	public static void sendSMS() throws ClientProtocolException, IOException {
		
		String smsContent = "gulf";
		String smsContent1 = URLEncoder.encode(smsContent, "UTF-8");
		RequestConfig requestConfig = RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD).build();
		CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig).build();
		HttpGet getRequest = new HttpGet("http://alerts.solutionsinfini.com/api/v4?api_key=Ab6404af37450ecc8fe6632286d32e731&method=sms&message=Gulf&to=9791514732&sender=CXWGLP&entity_id=1601100000000002444&template_id=&unicode=0");
		getRequest.addHeader("accept", "application/json");
		HttpResponse response = httpClient.execute(getRequest);
		
		StringBuffer sb = new StringBuffer();
		if(response.getEntity().getContent()!=null) {
			BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));
			String output;
			while ((output = br.readLine()) != null) {
				sb.append(output);
			}
		}else {
			sb.append("Empty Response from server.");
		}

		httpClient.close();
		String respMessage = sb.toString();
		System.out.println(respMessage);
		
		
		
		//return response;
	}
	
	public static void finalAmount(int months) {
		double princ = 75000;
		int initAmt =75000;
		int interest = 2;
		double intAmount =0;
		
		for(int i=1;i<months;i++) {
			intAmount = princ*(0.019);
			//System.out.println(intAmount);
			princ +=intAmount+initAmt;
//			System.out.println(princ/55);
			
		}
		System.out.println("===============================");
		System.out.println("===============================");
		System.out.println("===============================");
		System.out.println("===============================");
		System.out.println("===============================");
		System.out.println("===============================");
		System.out.println("Months invested:"+ months);
		System.out.println("Total Invested:"+ 1000*months);
		System.out.println("Returns after "+months+" months :"+(princ/75));
		System.out.println("Gain:"+((princ/75)-(1000*months)));
		System.out.println("===============================");
		
		
		
	}

}
