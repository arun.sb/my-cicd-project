package com.firstHivePOC;

import java.time.Duration;

import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.PersistentCacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.EntryUnit;
import org.ehcache.config.units.MemoryUnit;

public class MyCacheImplementation {

	
	public static Cache buildInMemoryCache(String cacheName, Class<?> ketType, Class<?> valueType, int timelimit){
        PersistentCacheManager persistentCacheManager = CacheManagerBuilder.newCacheManagerBuilder().with(CacheManagerBuilder.persistence("C:\\Users\\fh148\\caching_POC"))
        .withCache(cacheName, CacheConfigurationBuilder.newCacheConfigurationBuilder(ketType, valueType,
        ResourcePoolsBuilder.heap(100).disk(10, MemoryUnit.MB,true)).withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofSeconds(timelimit))))
        .build();

        
        persistentCacheManager.init();
        return persistentCacheManager.getCache(cacheName, ketType, valueType);
        }
	
}
