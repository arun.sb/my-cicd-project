package com.firstHivePOC;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;

import com.pepipost.api.Configuration;
import com.pepipost.api.PepipostClient;
import com.pepipost.api.controllers.MailSendController;
import com.pepipost.api.http.client.APICallBack;
import com.pepipost.api.http.client.HttpContext;
import com.pepipost.api.models.Attachments;
import com.pepipost.api.models.Content;
import com.pepipost.api.models.EmailStruct;
import com.pepipost.api.models.From;
import com.pepipost.api.models.Personalizations;
import com.pepipost.api.models.Send;
import com.pepipost.api.models.TypeEnum;

public class PepiPostAPI {

	public static void main(String[] args) throws ClientProtocolException, IOException {
		// sendMail();
		sendMailViaAPI();
	}

	public static void sendMail() throws ClientProtocolException, IOException {

		RequestConfig requestConfig = RequestConfig.custom().setCookieSpec(CookieSpecs.STANDARD).build();
		CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig).build();
		HttpPost request = new HttpPost("https://api.pepipost.com/v5/mail/send");
		request.addHeader("content-type", "application/json");
		request.addHeader("api_key", "46b4ba814bf2ee6e82eb45cd0b95dd4b");
		StringEntity params = new StringEntity(
				"{\"from\":{\"email\":\"arunsb@pepisandbox.com\",\"name\":\"arunsb\"},\"subject\":\"Your Barcelona flight e-ticket : BCN2118050657714\",\"content\":[{\"type\":\"html\",\"value\":\"Hello Lionel, Your flight for Barcelona is confirmed.\"}],\"personalizations\":[{\"to\":[{\"email\":\"arun.sb@firsthive.com\",\"name\":\"Lionel Messi\"}]}]}");
		request.setEntity(params);
		HttpResponse response = httpClient.execute(request);
		System.out.println(response);

	}

	public static void sendMailViaAPI() {

		try {
			PepipostClient client = new PepipostClient();
			MailSendController mailSendController = client.getMailSend();
			Configuration.apiKey = "bb1d28c47327c21ab30bd6ed32a1e6ae";
			Send body = new Send();

			body.setFrom(new From());
			body.getFrom().setEmail("arunsb@pepisandbox.com");
			body.getFrom().setName("Arun SB ");
			body.setSubject("Emailing with Pepipost is easy!!");
			body.setContent(new LinkedList<Content>());

			Content body_content_0 = new Content();
			body_content_0.setType(TypeEnum.HTML);
			body_content_0.setValue(
					"<html><body>Hey,<br><br>Do you know integration is even simpler in Pepipost, <br>with Java <br> Happy Mailing ! <br><br>Pepipost </body></html>");
			body.getContent().add(body_content_0);

			body.setPersonalizations(new LinkedList<Personalizations>());

			Personalizations body_personalizations_0 = new Personalizations();
			body_personalizations_0.setTo(new LinkedList<EmailStruct>());
			EmailStruct body_personalizations_0_to_0 = new EmailStruct();

			//cc & bcc
		//	EmailStruct cc = new EmailStruct();
			EmailStruct bcc = new EmailStruct();
		//	cc.setEmail("sibananda.sahoo@firsthive.com");
			bcc.setEmail("sbarun000@gmail.com");
		//	List<EmailStruct> ccEmail = new ArrayList<EmailStruct>();
	//		ccEmail.add(cc);
			List<EmailStruct> bccEmail = new ArrayList<EmailStruct>();
			bccEmail.add(bcc);
			body_personalizations_0.setBcc(bccEmail);
		//	body_personalizations_0.setCc(ccEmail);
			
			//Attachment
			List<Attachments> attchList = new ArrayList<Attachments>();
			String filePath = "C:\\Users\\fh148\\Documents\\Arun SB\\Pics\\firsthive-logo.png";
			byte[] input_file = Files.readAllBytes(Paths.get(filePath));
			byte[] encodedBytes = Base64.getEncoder().encode(input_file);
			String encodedString = new String(encodedBytes);
			Attachments attach = new Attachments();
			attach.setContent(encodedString);
			attach.setName("FirstHiveLogo.png");
			attchList.add(attach);
			
			//To
			body_personalizations_0_to_0.setName("ArunSb");
			body_personalizations_0_to_0.setEmail("arun.sb@firsthive.com");
			
			//Adding the personalization to body
			body_personalizations_0.getTo().add(body_personalizations_0_to_0);
			body_personalizations_0.setAttachments(attchList);
			body.getPersonalizations().add(body_personalizations_0);

			mailSendController.createGeneratethemailsendrequestAsync(body, new APICallBack<Object>() {
				public void onSuccess(HttpContext context, Object response) {

					System.out.println(response.toString());
					JSONObject jsonData = new JSONObject(response.toString());
					System.out.println(jsonData.get("data"));
					System.out.println(context.getResponse().getStatusCode());

				}

				public void onFailure(HttpContext context, Throwable error) {

					System.out.println(error.getMessage());
					System.out.println(context.getResponse().getStatusCode());

				}
			});

		} catch (Exception e) {
			e.printStackTrace();

		}

	}
}
