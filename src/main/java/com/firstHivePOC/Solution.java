package com.firstHivePOC;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Solution {
	   public static void main(String[] args) {
		    
		    
	        List<List<double[]>> calendarList = new ArrayList<List<double[]>>();
	        Scanner sc = new Scanner(System.in);
	        int t = sc.nextInt();
	        
	        
	        for (int n = 0; n < t; n++) {
	            List<double[]> dayList = new ArrayList<double[]>();
	            int classes = sc.nextInt();
	            while(classes>0){
	                double[] cal = new double[2];
	            String className = sc.next();
	            String startTime = sc.next();
	            String endTime = sc.next();
	            cal[0] = Double.parseDouble(startTime.replace(":", "."));
	            cal[1] = Double.parseDouble(endTime.replace(":", "."));
	            dayList.add(cal);
	            classes--;
	            }
	            calendarList.add(dayList);
	        }
	            System.out.println(calendarList.get(0).get(2)[0]);
	    }
}
